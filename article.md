---
title: "Map of heat in Prague: Why is it more pleasant in Vinohrady than in Smíchov in the summer?"
perex: "The capital city is looking for ways to deal with tropical heat"
description: "The capital city is looking for ways to deal with tropical heat."
authors: ["Jan Cibulka"]
published: "10. srpna 2016"
socialimg: https://interaktivni.rozhlas.cz/horko-ve-mestech/media/socimg.png
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "horko-ve-mestech-en"
libraries: [jquery, highcharts,  https://interaktivni.rozhlas.cz/tools/highcharts-technical-indicators/0.0.1.min.js, https://js.arcgis.com/3.17/]
styles: [https://js.arcgis.com/3.17/esri/css/esri.css]
recommended:
  - link: https://interaktivni.rozhlas.cz/horko-v-brne/
    title: Map of hot Brno: It is up to ten degrees warmer at stations and in industrial zones
    perex: On summer days, industrial zones, stations and shopping centres heat up to temperatures as much as ten degrees higher than those in parks and grassy areas. This can be seen in pictures from the Landsat 8 satellite, which enable estimation of the surface temperature.
    image: https://interaktivni.rozhlas.cz/horko-v-brne/media/socimg.png
---

In Náměstí Republiky on the hottest days of summer, the temperature reaches forty degrees Celsius even before noon and the temperature at neighbouring Masaryk Station approaches fifty degrees. At the same time, the not-too-far-away Riegerovy sady park is enjoying a pleasant thirty degrees. Prague is now beginning to look for ways to take the pressure off the overheated central parts. It is already clear that trees are a great help.

Tropical days, i.e. days on which the maximum daily temperature exceeds 30 °C, are becoming increasingly frequent in the Czech Republic. The meteorological station at Ruzyně in Prague recorded 24 tropical days last year. According to the [calculations](https://www.natur.cuni.cz/geografie/fyzicka-a-geoekologie/aktuality/sucho-v-ceskych-zemich-minulost-soucasnost-a-budoucnost) of scientists at the Academy of Sciences, who employed a modified [ALADIN](https://cs.wikipedia.org/wiki/ALADIN), meteorological model, this is expected to increase by half as many by 2050.

In large cities, concrete or asphalt areas create heat islands that radiate heat even at night. On an average it is 2.4 °C warmer at night in Prague than in the countryside. Higher temperatures make the lives of inhabitants unpleasant and are [detrimental to health](http://www.rozhlas.cz/pardubice/zpravodajstvi/_zprava/zachranari-maji-pri-horkych-letnich-dnech-vic-prace-nez-obvykle--1513246).

The increasing trend is apparent from the survey of tropical days in Prague, whose number has increased by about one third since 1775. In recent times, extreme temperatures were recorded in 1994 and 2003, during which the temperature increased to over thirty degrees on almost thirty days.

<aside class="big">
   <div style="height:400px;" class="trop_pha"></div>
</aside>

Czech Radio measured temperature differences right in the centre of Prague. On the basis of pictures from the [Landsat 8](http://landsat.usgs.gov/landsat8.php) satellite, an estimate was made of the temperatures of urban surfaces during the morning of 24 June 2016. This specific date was selected because of the high daytime temperature and minimum cloud cover, so that clouds did not obscure the view from the satellite.

*The satellite is capable of measuring [electromagnetic radiation](https://cs.wikipedia.org/wiki/Elektromagnetick%C3%A9_spektrum) from various types of surfaces. Subsequent calculations permit approximate determination of their temperatures at the time the picture was taken. The determined temperatures are higher than those we know from weather forecasts: meteorological thermometers measure the temperature in the shade while the satellite measured the approximate values of the roofs and roads exposed to the sun.*

<aside class="small"> <figure> <img src="./media/scrn_nadrazi.jpg" width="300"> </figure> <figcaption><i>The overheated surroundings of Masaryk Station in contrast to the nearby park</i></figcaption> </aside>

As can be seen in the following map, the worst conditions exist on large dark surfaces, such as [brownfields](https://cs.wikipedia.org/wiki/Brownfield), i.e. in a city typically a railway station, and also on parking lots or the roofs of factory halls.

On the other hand, it is cooler near rivers and close to trees. The Vltava River is capable of cooling buildings in its vicinity by up to four degrees and this can be felt by people living in the second or even third row of houses away from the banks of the river. Even small trees and grass between buildings can manage to reduce the temperature by two or three degrees. This is why it is by about 4 °C more pleasant in Vinohrady than in Smíchov. Large green areas, like parks with trees, can maintain a temperature that is up to eight degrees lower than among the buildings in their vicinity.

<aside id="mapa" class="big">
	<div id="search"></div>
	<div style="height:600px;" id="map">
</aside>

The Capital City is now investigating ways of combating rising temperatures. In March of this year, Prague [joined](http://www.praha.eu/jnp/cz/o_meste/magistrat/tiskovy_servis/tiskove_zpravy/pristoupeni_hlavniho_mesta_prahy_k.html) the Mayors Adapt initiative, where an answer to the same problems is also being sought by [Brno](http://urbanadapt.cz/cs/adaptace-mesta-brna-na-klimaticke-zmeny) and [Plzeň](http://urbanadapt.cz/cs/adaptace-mesta-plzne-na-klimaticke-zmeny). In Prague, this project is the responsibility of the [Institute of Planning and Development](http://www.iprpraha.cz/), which is expected to present its [results](http://www.iprpraha.cz/adaptacnistrategie) in the first half of 2017. Of the proposed measures, politicians will then select the projects into which the city intends to invest.

Until that time, Prague must be satisfied with its [strategic plan](http://www.iprpraha.cz/clanek/83/co-je-strategicky-plan), which anticipates planting more green areas. In addition to combating heat, trees also contribute to reducing dust levels and generally make life outdoors more pleasant for the inhabitants of Prague. The new [construction regulations](http://www.iprpraha.cz/coprinasipsp) already require that every street that is more than 12 metres wide be planned so that it is possible to plant a row of trees. This is especially relevant for the location of utility networks; pipelines and cables must be sufficiently deep to avoid tree roots.

The situation is more complicated in older built-up areas where the relaying of all the pipelines would be prohibitively expensive. Thus tests are being carried out of planting trees in troughs in Šlikova street in Břevnov. In contrast to the infamous [concrete troughs in Smíchov](http://www.rozhlas.cz/zpravy/regiony/_zprava/na-prazsky-smichov-se-vrati-kontroverzni-truhliky-budou-mit-novou-podobu--1501194), these are made of wood and are substantially smaller, so that they do not create an obstruction on sidewalks.

<aside class="big"> <figure> <img src="./media/slikova_praha.png" width="1000"> </figure> <figcaption>Troughs in Šlikova street, foto <a href="https://mapy.cz/s/Xjoc" target="_blank">Mapy.cz</a> </figcaption> </aside>

<aside class="small"> <figure> <img src="./media/parkoviste_plzen.jpg" width="300"> </figure> <figcaption><a href="http://urbanadapt.cz/cs/publikace-adaptace-na-zmenu-klimatu-ve-mestech" target="_blank">Adaptation to climate change in cities, p. 78</a> </figcaption> </aside>

At some places, the design engineers will replace the dark-coloured asphalt, which easily heats up to temperatures over 50 °C, with other materials. Options include lighter-coloured concrete or the familiar concrete paving stones with openings, used in the example of reconstruction of Štruncové sady in Plzeň. Grass can grow through them and excess water sinks in. In addition to heat, these parking lots can better deal with sudden downpours.

The Capital City has now opened a new [Max van der Stoel park](http://www.praha.eu/jnp/cz/o_meste/zivot_v_praze/zivotni_prostredi/velky_park_slouzi_nejen_detem.html) in Prague 6. The Brusnice stream flows between trees and grass and empties into the local lake. The children’s playground is designed so that the children can easily reach the water. The prepared [adaptation strategy](http://www.iprpraha.cz/adaptacnistrategie) and especially interest on the part of Prague citizens indicate whether other Prague parks will look similar.

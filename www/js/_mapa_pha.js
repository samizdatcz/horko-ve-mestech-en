var map, identifyTask, identifyParams;
require([
  "esri/map",
  "esri/layers/ArcGISImageServiceLayer",
  "esri/symbols/SimpleFillSymbol",
  "esri/symbols/SimpleLineSymbol",
  "esri/InfoTemplate",
  "esri/tasks/ImageServiceIdentifyTask",
  "esri/tasks/IdentifyParameters",
  "esri/dijit/Popup",
  "esri/Color",
  "dojo/dom-construct",
  "dojo/_base/array",
  "esri/layers/ImageServiceParameters",
  "dojo/parser",
  "esri/dijit/Search", 
  "dojo/domReady!"
], function(
  Map,
  ArcGISImageServiceLayer,
  SimpleFillSymbol,
  SimpleLineSymbol,
  InfoTemplate,
  ImageServiceIdentifyTask,
  IdentifyParameters,
  Popup,
  Color,
  domConstruct,
  arrayUtils,
  ImageServiceParameters, 
  parser,
  Search
) {
  parser.parse();

  var servURL = "https://datazurnal.cz/arcgis/rest/services/vedro_praha/ImageServer";

  var popup = new Popup({
    fillSymbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
      new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
        new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.25]))
  }, domConstruct.create("div"));

  function mapReady () {
    map.on("click", executeIdentifyTask);
    identifyTask = new ImageServiceIdentifyTask(servURL);

    identifyParams = new IdentifyParameters();
    identifyParams.tolerance = 1;
    identifyParams.returnGeometry = true;
    identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
    identifyParams.width = map.width;
    identifyParams.height = map.height;
  }

  function executeIdentifyTask (event) {
    identifyParams.geometry = event.mapPoint;
    identifyParams.mapExtent = map.extent;

    var pixVal;

    var deferred = identifyTask.execute(identifyParams);
      deferred.addCallback(function (response) {
        pixVal = response.value;
        map.infoWindow.setTitle("Estimated surface temperature at<br> 24. June 2016 forenoon");
        map.infoWindow.setContent(
            Math.round(pixVal * 10)/10 + ' °C'
          );
      map.infoWindow.show(event.mapPoint);
      });
    };

  map = new Map("map", {
    basemap: "hybrid",
    center: [14.446678, 50.068709],
    zoom: 11
  });


  var search = new Search({
    map: map,
    autoNavigate: false
  },"search");
    
  search.startup();

  var params = new ImageServiceParameters();
  params.noData = 0;
  var imageServiceLayer = new ArcGISImageServiceLayer(servURL, {
    imageServiceParameters: params,
    opacity: 0.55
  });
  map.addLayer(imageServiceLayer);
  map.on("load", mapReady);
});
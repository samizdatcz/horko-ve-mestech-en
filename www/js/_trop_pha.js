$(function () {
		var tempData = [6, 2, 0, 7, 0, 0, 12, 11, 3, 4, 2, 0, 7, 7, 3, 3, 12, 7, 6, 6, 3, 3, 7, 10, 3, 6, 1, 11, 7, 7, 0, 0, 21, 9, 7, 2, 18, 0, 0, 0, 0, 0, 0, 0, 3, 2, 
		0, 1, 1, 0, 1, 4, 8, 5, 6, 12, 0, 4, 3, 24, 7, 2, 3, 2, 3, 0, 4, 8, 0, 0,
		 3, 4, 1, 1, 2, 1, 0, 4, 6, 2, 4, 1, 11, 3, 17, 2, 11, 5, 6, 0, 11, 3, 4, 24, 7, 7, 3, 5, 9, 10, 5, 5, 7, 3, 0, 3, 7, 0, 3, 3, 10, 6, 4, 2, 4, 4, 2, 15,
		  1, 3, 4, 1, 2, 6, 2, 6, 2, 0, 0, 8, 7, 4, 3, 5, 0, 0, 12, 0, 1, 0, 4, 0, 8, 3, 2, 2, 14, 1, 4, 3, 2, 1, 5, 12, 13, 11, 9, 12, 8, 8, 13, 2, 6, 9, 6, 2, 4, 11, 12, 8, 11, 9,
		   27, 14, 11, 13, 7, 19, 13, 5, 2, 1, 10, 3, 5, 3, 4, 5, 12, 16, 6, 4, 17, 5, 13, 9, 17, 9, 7, 5, 3, 11, 2, 1, 6, 2, 5, 9, 16, 5, 7, 12, 2, 10, 9, 14, 7, 19, 7, 27, 13, 4, 6, 9, 8, 14,
		    11, 13, 28, 10, 3, 18, 7, 6, 6, 8, 1, 9, 7, 7]

		var years = [];

		for (i = 1775; i < 2015; i++) { 
    		years.push(i);
		};

    $('.trop_pha').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Number of tropical days in Prague'
        },
        subtitle: {
        	align: 'right',
            verticalAlign: 'bottom',
            x: 0,
            y: -5,
            text: '<i style="font-size:9px"><a target="_blank" href="https://www.ncdc.noaa.gov/cdo-web/">NOAA archive</a>; timeline is not complete,' + 
            '<br>data from <a target="_blank" href="http://portal.chmi.cz/">Czech Hydrometeorological Institute</a> are not publicly available</i>',
            useHTML: true
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: years,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of days'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">Rok {point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">Tropical days: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Tropical days (maximal temperature 30 °C and more)',
            id: 'dataset',
            data: tempData
        }, {
        	name: 'Trend',
            linkedTo: 'dataset',
            showInLegend: false,
            enableMouseTracking: false,
            type: 'trendline',
            color: 'red',
            dashStyle: 'dot',
            algorithm: 'linear'
        }]
    });
});